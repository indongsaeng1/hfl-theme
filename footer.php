		<!-- Footer nav -->
		<div id="footer">
			<nav><?php footer_nav(); ?></nav>
			<div class="copyright">
				&copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?>
			</div>
		</div>
	</div>
	<?php wp_footer(); ?>
	<!-- Don't forget analytics -->
</body>
</html>
